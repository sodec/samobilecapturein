# SAMobileCapture #

### 0 - Örnek proje Swift ###

[Örnek proje](https://bitbucket.org/sodec/samobilecapturein/src/master/MobileCaptureSwift.zip "indir")

### 0 - Örnek proje Obj-C ###

[Örnek proje](https://bitbucket.org/sodec/samobilecapturein/src/master/MobileCapture.zip "indir")


## 1 - Sodec Chip Reader ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : Controllers/SAMobileCaptureModules/SAChipReadExample.swift

### 1a - Proje ayarları  ###

- Entitlements oluşturmak için; Proje target / Signing & Capabilities / + Capability (çıkan listeden Near Field Communication Tag Reading seçilir) içeriği aşağıdaki gibi olan Projeniz.entitlements isminde bir dosya oluşturacaktır. Projeyi yayına alırken Apple sizden NDEF kımını silmenizi isteyebilir.

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>com.apple.developer.nfc.readersession.formats</key>
    <array>
        <string>NDEF</string>
        <string>TAG</string>
    </array>
</dict>
</plist>
```

- Info.plist dosyasına aşağıdaki parametreler eklenmelidir; 
```
<key>com.apple.developer.nfc.readersession.iso7816.select-identifiers</key>
    <array>
        <string>A0000002471001</string>
    </array>
    <key>NFCReaderUsageDescription</key>
    <string>Kimlik okuma açıklaması</string>
```

- Bu repoda bulunan örnek proje içerisinde “masterList.pem” isminde bir sertifika bulunur onu projenie ekleyin. Resmi kurumlar tarafından yaratılmış bir sertifikadır kimlik chip doğrulaması için gereklidir.


### 1b - SAReadChip init edilmesi ve modülün başlatılması için gerekli kodlar aşağıdadır.  ###

- folioPageDidDone fonksiyonu içerisinde yeni bir alan olan expireDate eklenmiştir. 
- SAReadChip init edilirken beklenen MRZ anahtarı içerisinde kullanılan birthDate expiryDate alanlarının formatı kimliğin arka kısmında bulunan MRZ alanında olduğu gibi YYMMDD şeklinde olmalıdır örneğin 08 Ocak 1977 olan tarih 770108 şeklinde formatlanmalıdır ay gün tek haneli ise başına 0 eklenmelidir. 


```
           NSString *  bundlefile = [[NSBundle mainBundle] pathForResource:@"masterList" ofType:@"pem"];
           
           SAReadChip *sAReadChip = [[SAReadChip alloc ] init];
           
           
           [sAReadChip setParamWithNfcInfoTittle:SALocalizedString(@"*NFC Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz.", @"Please touch your NFC chip enabled id card top of your device")];
           
           [sAReadChip setParamWithNfcReadingTittle:SALocalizedString(@"*Lütfen kartınızı haraket ettirmeyin", @"Please do not move your card")];
           
           [sAReadChip setParamWithValidateDocumentTittle:SALocalizedString(@"*Döküman doğrulanıyor" , @"Verifing document")];
           
           [sAReadChip setParamWithInvalidTagTittle:SALocalizedString(@"*Geçersiz tag." , @"Invalid tag.")];
          
           [sAReadChip setParamWithMultiTagTittle:SALocalizedString(@"*Birden fazla tag bulundu lütfen aynı anda sadece bir tag okutun." , @"Found multiple tag. Please read one tag at a time.")];
 
           [sAReadChip setParamWithConnectionErrorTittle:SALocalizedString(@"*Bağlantı hatası. Lütfen tekrar deneyin." , @"Connection error. Try again.")];
           
           [sAReadChip setParamWithMrzErrorTittle:SALocalizedString(@"*MRZ anahtar uyumsuzdur." , @"MRZ key is not valid")];
           
           [sAReadChip setParamWithReadingErrorTittle:SALocalizedString(@"*Döküman okuma hatası." , @"Document read error.")];
           
           [sAReadChip setParamWithReadingSuccessTittle:SALocalizedString(@"*Döküman okuma başarılı." , @"Document read successfull")];
           
           [sAReadChip setParamWithCorruptedDataTittle:SALocalizedString(@"Part of returned data may be corrupted" , @"Part of returned data may be corrupted")];
           
           [sAReadChip setParamWithFileInvalidatedTittle:SALocalizedString(@"file invalidated" , @"file invalidated")];
           
           [sAReadChip setParamWithFCIErrorTittle:SALocalizedString(@"FCI not formatted according to ISO7816-4 section 5.1.5" , @"FCI not formatted according to ISO7816-4 section 5.1.5")];
           
           [sAReadChip setParamWithTransmissionErrorTittle:SALocalizedString(@"Secured Transmission not supported" , @"Secured Transmission not supported")];
           
           [sAReadChip setParamWithMemoryErrorTittle:SALocalizedString(@"Memory failure" , @"Memory failure")];
           
           [sAReadChip setParamWithCommandErrorTittle:SALocalizedString(@"Invalid command" , @"Invalid command")];
           

```

```
[sAReadChip setParamWithAuthenticationErrorTittle:SALocalizedString(@"Authentication error" , @"Authentication error")];
     
     

     [sAReadChip setParamWithInvalidDataErrorTittle:SALocalizedString(@"Invalid data" , @"Invalid data")];
     
     [sAReadChip readChipWithIdNumber:self->idSerialNumber dateOfBirth:self->birthDate expiryDate:self->expireDate delegate:self masterListURL:[NSURL URLWithString:bundlefile]];

```



### 1c - SAChipDelegate ile dönecek olan fonksiyonlar aşağıdaki gibidir.  ###
```
- (void)didReadChipDoneWithSaChipData:(SAChipData * _Nonnull)saChipData {
    NSLog(@"SAChipData activeAuthenticationSupported : %id",saChipData.activeAuthenticationSupported);
    UIImage *passportImage = saChipData.passportImage;
    UIImage *signatureImage = saChipData.signatureImage;
    NSLog(@"SAChipData activeAuthenticationPassed : %id",saChipData.activeAuthenticationPassed);
    NSLog(@"SAChipData passportDataNotTampered : %id",saChipData.passportDataNotTampered);
    NSLog(@"SAChipData documentSigningCertificateVerified : %id",saChipData.documentSigningCertificateVerified);
    NSLog(@"SAChipData passportCorrectlySigned : %id",saChipData.passportCorrectlySigned);
    NSLog(@"SAChipData lastName : %@",saChipData.lastName);
    NSLog(@"SAChipData firstName : %@",saChipData.firstName);
    NSLog(@"SAChipData nationality : %@",saChipData.nationality);
    NSLog(@"SAChipData gender : %@",saChipData.gender);
    NSLog(@"SAChipData dateOfBirth : %@",saChipData.dateOfBirth);
    NSLog(@"SAChipData documentExpiryDate : %@",saChipData.documentExpiryDate);
    NSLog(@"SAChipData issuingAuthority  : %@",saChipData.issuingAuthority );
    NSLog(@"SAChipData documentNumber : %@",saChipData.documentNumber);
    NSLog(@"SAChipData personalNumber : %@",saChipData.personalNumber);
    NSLog(@"SAChipData documentSubType : %@",saChipData.documentSubType);
    NSLog(@"SAChipData documentType : %@",saChipData.documentType);
 
}
 
- (void)didReadChipCancel {
    dispatch_async(dispatch_get_main_queue(), ^{
     
   
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"NFC Error"
                                   message:[NSString stringWithFormat:@"Kullanıcı iptal etti"]
                                   preferredStyle:UIAlertControllerStyleAlert];
 
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"KAPAT" style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {}];
 
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
    });
    
 
}
 
 
- (void)didReadChipDoneWithErrorWithError:(NSError * _Nonnull)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"didReadChipDoneWithErrorWithError :%@",[NSString stringWithFormat:@"%@",error.userInfo]);
  
    });
    
}

```      
        
  
## 2 - Folio ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : Controllers/SAMobileCaptureModules/SAFolioPageExample.swift

### 2a - Folio kullanımı ###

- Proje config ayarlarını AppDelegate içerisinde "didFinishLaunchingWithOptions" fonksiyonu içerisinde başlatabilirsiniz kod kullanımına örnek proje içersinden bakabilirsiniz 
                   
```   
        let mainColor = UIColor.black
        let subColor = UIColor.black
        // do not forget to add your fonts to your Info.plist
        let lightFontName = "Quicksand-Light"
        let regularFontName = "Quicksand-Regular"
        let boldFontName = "Quicksand-Bold"
        // NOTE 4: ----- change here -----

        let config = SAConfig.create()
        // NOTE 5: set NO in the release mode, this is so important
        config?.debuggable = true
        // you can set it as English like @"en"
        config?.language = "tr"

        // NOTE 6: you do not need to change these default configs, but you can change them whenever you want
        let uiConfig = SAUIConfig.create()
        uiConfig?.customStatusBarImage = nil
        uiConfig?.customViewImage = nil
        uiConfig?.customBottomBGImageForX = nil
        uiConfig?.statusBarColor = mainColor
        uiConfig?.navBarColor = mainColor
        uiConfig?.viewColor = UIColor(red: 242.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
        uiConfig?.toolBarColor = mainColor
        uiConfig?.bottomBGColorForX = mainColor
      
        uiConfig?.regularFont = regularFontName
        uiConfig?.boldFont = boldFontName
        uiConfig?.barTranslucent = false
        uiConfig?.navBarTitleFontSize = 19.0
        uiConfig?.barButtonItemTitleFontSize = 18.0
        
        let cropConfig = SACropConfig.create()
        cropConfig?.cropBackgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.25)
        cropConfig?.cropButtonSize = 40.0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            cropConfig?.cropButtonSize = cropConfig?.cropButtonSize ?? 0.0 * 1.5
        }
        cropConfig?.cropButtonColor = mainColor
        cropConfig?.cropValidLineColor = mainColor
        cropConfig?.cropInvalidLineColor = UIColor(red: 248.0 / 255.0, green: 65.0 / 255.0, blue: 45.0 / 255.0, alpha: 1.0)
        cropConfig?.cropActiveButtonColor = mainColor.withAlphaComponent(0.25)
        cropConfig?.cropLineWidth = 5.0
        cropConfig?.cropZoomBackgroundColor = UIColor.black
        cropConfig?.cropZoomSize = cropConfig?.cropButtonSize ?? 0.0 * 2.0
        cropConfig?.cropZoomScale = 3.0
        cropConfig?.cropZoomBorderColor = UIColor.gray
        cropConfig?.cropZoomBorderWidth = 5.0

        let scanConfig = SAScanConfig.create()
        scanConfig?.scanBackgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        scanConfig?.scanRectangleLineColor = mainColor
        scanConfig?.scanRectangleLineWidth = 2.0
        scanConfig?.scanAngleSize = 24.0
        scanConfig?.scanAngleLineWidth = 6.0
        scanConfig?.scanAngleColor = mainColor

    
        let alertConfig = SAAlertConfig.create()
        alertConfig?.alertViewIndicatorColor = mainColor
        // NOTE 7: add these images to your project

        alertConfig?.alertViewTitleFontFamily = boldFontName
        alertConfig?.alertViewBodyFontFamily = regularFontName
        alertConfig?.alertViewButtonsFontFamily = boldFontName
        alertConfig?.alertViewTitleFontSize = 18.0
        alertConfig?.alertViewBodyFontSize = 16.0
        alertConfig?.alertViewButtonsFontSize = 16.0
        alertConfig?.alertViewButtonsCornerRadius = 15.0

        let toastConfig = SAToastConfig.create()
        toastConfig?.toastBackgroundColor = mainColor
        toastConfig?.toastTitleFontFamily = boldFontName
        toastConfig?.toastBodyFontFamily = boldFontName
        toastConfig?.toastTitleFontSize = 18.0
        toastConfig?.toastBodyFontSize = 16.0
        toastConfig?.toastDuration = 3.0
        toastConfig?.toastShouldDismissOnClick = false

        let indicatorConfig = SAIndicatorConfig.create()
        indicatorConfig?.indicatorColor = UIColor.white
        indicatorConfig?.indicatorBackgroundColor = mainColor
        indicatorConfig?.indicatorSize = 50.0
        indicatorConfig?.indicatorCornerRadius = 5.0
        indicatorConfig?.indicatorDIMValue = 0.4

        let progressConfig = SAProgressConfig.create()
        progressConfig?.progressBackgroundColor = mainColor
        progressConfig?.progressTitleFontFamily = boldFontName
        progressConfig?.progressBodyFontFamily = boldFontName
        progressConfig?.progressTitleFontSize = 18.0
        progressConfig?.progressBodyFontSize = 16.0
        progressConfig?.progressDIMValue = 0.4

        let notificationConfig = SANotificationConfig.create()
        notificationConfig?.notificationBackgroundColor = mainColor
        notificationConfig?.notificationTitleFontFamily = boldFontName
        notificationConfig?.notificationBodyFontFamily = regularFontName
        notificationConfig?.notificationTitleFontSize = 16.0
        notificationConfig?.notificationBodyFontSize = 14.0
        notificationConfig?.notificationDuration = 4.0
        notificationConfig?.notificationDurationPositiveDeviation = 1.0
        notificationConfig?.notificationDurationNegativeDeviation = 1.0
        notificationConfig?.notificationShouldDismissOnClick = true

        let stepperConfig = SAStepperConfig.create()
        stepperConfig?.stepperCircleColor = UIColor.white
        stepperConfig?.stepperIndicatorColor = mainColor
        stepperConfig?.stepperLineColor = UIColor.white
        stepperConfig?.stepperLineDoneColor = mainColor
        stepperConfig?.stepperNumberColor = UIColor.white
        stepperConfig?.stepperNumberFontFamily = boldFontName
        stepperConfig?.stepperNumberFontSize = 16.0
        stepperConfig?.stepperDescriptionColor = UIColor.white
        stepperConfig?.stepperDescriptionFontFamily = boldFontName
        stepperConfig?.stepperDescriptionFontSize = 16.0

        let counterConfig = SACounterConfig.create()
        counterConfig?.counterEnabled = true
        counterConfig?.counterBackgroundColor = UIColor(red: 232.0 / 255.0, green: 50.0 / 255.0, blue: 37.0 / 255.0, alpha: 1.0)
        counterConfig?.counterContentColor = UIColor.white
        counterConfig?.counterTitleFontFamily = boldFontName
        counterConfig?.counterTitleFontSize = 18.0
        counterConfig?.counterCornerRadius = 5.0
        counterConfig?.counterDuration = 10.0

        let galleryConfig = SAGalleryConfig.create()
        galleryConfig?.galleryEnabled = false
        galleryConfig?.galleryShowLastKnownImage = true
        galleryConfig?.galleryBorderColor = UIColor.clear
        galleryConfig?.galleryBorderWidth = 0.0
        galleryConfig?.galleryCornerRadius = 3.0

        // if your project supports asynchronous request, set it no. otherwise, set it yes
        galleryConfig?.synchronousRequest = false

        // NOTE 8: check the library is loaded successfully or not before using the library
        if SALibrary.isLibraryLoaded() {
            print("Library name: \(SALibrary.getName())")
            print("Library version code: \(SALibrary.getVersionCode())")
            print("Library version name: \(SALibrary.getVersionName())")
        }
```   


- Folio document modülünü başlatmak için gerekli kodlar aşağıdadır.

```  
        let tipConfig = SATipConfig.create()
        tipConfig?.tipEnabled = true
        
   
        let undesiredIdentityTypes = SAIdentityTypes()

        let folioPage = SAFolioPage()
        folioPage.delegate = self
        folioPage.folioPageDescription = "Lütfen kimliğinizin ön yüzünü çekiniz. Kimlik olarak \(SAIdentityUtility.getNameOfDesiredIdentityTypes(undesiredIdentityTypes)!) kullanılabilir. "
        folioPage.folioPageNavBarTitle = SALocalizedString("Kimlik", "Identity")
        folioPage.folderNameForFolioPage = SA_FOLIO_PAGE_FILE_NAME
        folioPage.frontCaptureDescription = "Lütfen kimliğinizin ön yüzünü çekiniz. Kimlik olarak \(SAIdentityUtility.getNameOfDesiredIdentityTypes(undesiredIdentityTypes)!) kullanılabilir. "
        folioPage.frontCaptureNavBarTitle = SALocalizedString("Ön Yüz Ekle", "Add front page")
        folioPage.frontProcessNavBarTitle = SALocalizedString("Düzenle", "Edit")
        folioPage.frontVerifyNavBarTitle = SALocalizedString("Onayla", "Confirm")
        folioPage.backCaptureNavBarTitle = SALocalizedString("Arka Yüz Ekle", "Add back page")
        folioPage.backProcessNavBarTitle = SALocalizedString("Düzenle", "Edit")
        folioPage.backVerifyNavBarTitle = SALocalizedString("Onayla", "Confirm")
    
        folioPage.showProgress = true
        folioPage.showIndicator = true
    
        let detectDocument = SADetectDocument()
        detectDocument.enabled = true
        detectDocument.minCountOfLetters = 50
        folioPage.detectDocument = detectDocument

        folioPage.blurDetection = true

        folioPage.classification = true

        folioPage.faceDetection = true
        folioPage.imageEnchancing = .none
        folioPage.encryptFile = true
        folioPage.showDocumentName = true

        folioPage.desiredIdentityNumber = nil
        
        folioPage.undesiredIdentityTypes = undesiredIdentityTypes
        folioPage.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        folioPage.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        present(folioPage, animated: true) 
```   


- SAFolioPage üzerinde sırasıyla aşağıdaki delegate fonksiyonları dönmektedir.

``` 
    func folioPageDidCancel(_ controller: SAFolioPage!) {}
    
    func folioPageDidClear(_ controller: SAFolioPage!) {}
    
    func folioPageDidDone(_ controller: SAFolioPage!, with identityData: SAIdentityData!, withFrontPagePath frontPagePath: String!, withBackPagePath backPagePath: String!, withFolioPagePath folioPagePath: String!, withIsEncrypted isEncrypted: Bool) {}
    
    func folioPageError(_ error: Error!) {}
    
``` 


## 3 - Selfie ve Video ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : Controllers/SAMobileCaptureModules/SASelfieAndVideoExample.swift

### 3a - Selfie ve Video kullanımı ###

- Selfie ve Video adımlarını ayrı ayrı kullanabilirsiniz veya listenerlara cevap vererek bir sonraki akışa otomatik geçmesini sağlayabilirsiniz. Her iki kullanım türü de örnek proje içerisine eklenmitir. 

- SACaptureSelfie kullanımı:

``` 
    let tipConfig = SATipConfig.create()
    tipConfig?.tipEnabled = true
    let captureSelfie = SACaptureSelfie()
    captureSelfie.delegate = self
    captureSelfie.selfieNavBarTitle = SALocalizedString("Selfie Çek", "Take a selfie")
    captureSelfie.folderNameForSelfie = SA_SELFIE_PAGE_FILE_NAME
    // the number of steps to verify liveness of the person
    captureSelfie.numberOfSteps = 2

    // to show what the user should do in the selected steps, set gifs files
    let gifParams = SAGIFParams()
    gifParams.enabled = true
    gifParams.introFileName = "intro.gif"
    gifParams.turnHeadRightFileName = "turn_right.gif"
    gifParams.turnHeadLeftFileName = "turn_left.gif"
    gifParams.blinkEyesFileName = "blink_eyes.gif"
    captureSelfie.gifParams = gifParams

    
    // use SD quality for screen recording. otherwise, the size of the video may be big
    captureSelfie.screenRecordQuality = .standard
    // you may use medium quality to not give memory warning in the old devices
    captureSelfie.captureQuality = .high
    // to get a better result in emotion detection, use accurate mode rather than fast mode
    captureSelfie.faceMode = .accurate
    // to warn the user while the screen is recording
    captureSelfie.showScreenRecordActiveIcon = true
    // to warn the user that audio is not recording in the screen record
    captureSelfie.showScreenRecordMuteIcon = false
    captureSelfie.encryptFile = true

    // NOTE 16: add these codes to capture Video - change strings to customize it
    let videoCaptureParams = SACaptureVideoParams()
    // If you want to capture a video for audible confirmation, please set it YES
    // Otherwise, set it NO to capture only selfie video
    videoCaptureParams.enabled = true
    videoCaptureParams.videoNavBarTitle = SALocalizedString("Video Kaydet", "Record video")
    // use the same file name with selfie
    videoCaptureParams.folderNameForVideo = SA_SELFIE_PAGE_FILE_NAME
    // to show what the user should say for voice authorization
    videoCaptureParams.desiredSentence = SALocalizedString("Vermiş olduğum bilgilerin doğruluğunu ve belgelerin bana ait olduğunu onaylıyorum.", "Desired sentence")
    // to explain how the user can use the button for video recording
    videoCaptureParams.recordButtonDescription = SALocalizedString("Sesli yönlendirme bittikten sonra Kayıt Tuşu'nu kullanarak kaydınızı gerçekleştirebilirsiniz.", "Record button description")

    let ttsParams = SATTSParams()
    // if you want to use a custom voice for text to speech, set it yes. otherwise, the default speech engine of the phone will be used
    ttsParams.customTTS = false
  
    // if you want to use a custom voice, do not forget to add the audio file of this custom voice to your project
    let config = SAConfig.create()
    config?.language = "tr"
    ttsParams.customTTSFileName = ((config?.language == "en") ? "custom_male_tts_en.mp3" : "custom_male_tts_tr.mp3")
    // what the tts engine should say to direct the user
    ttsParams.ttsSentence = SALocalizedString("Lütfen kayıt tuşuna tıkladıktan sonra ekranın altında yer alan cümleyi sesli olarak ve kameraya bakarak söyleyiniz.", "TTS sentence")
    // the baseline pitch at which the utterance will be spoken
    ttsParams.pitch = 1.0
    // the rate at which the utterance will be spoken. use the default speech rate of the phone. you should add AVFoundation framework to use it
    ttsParams.speechRate = AVSpeechUtteranceDefaultSpeechRate
    videoCaptureParams.ttsParams = ttsParams

    // use WAV format for the speech recognition service
    videoCaptureParams.audioFormat = .WAV
    // use SD quality for video recording. otherwise, the size of the video may be big
    videoCaptureParams.videoQuality = .standard
    // to detect face in the recorded video
    videoCaptureParams.faceDetectionInVideo = true
    captureSelfie.captureVideoParams = videoCaptureParams

    // If you want to capture a selfie video for verifying human, please set captchaMode YES
    let captcha = SACaptcha()
    // If you enable captchaMode, the numberOfSteps variable will be two
    // And these two steps are turnHeadRight and turnHeadLeft
    captcha.enableCaptchaMode = false
    // If you enable captchaMode, you can dismiss the face recognition listener by setting enableFaceRecognition NO
    // If captchaMode is not enabled, the enableFaceRecognition variable will not work
    captcha.enableFaceRecognition = false
    captureSelfie.captcha = captcha
    captureSelfie.modalTransitionStyle = UIModalTransitionStyle.coverVertical
    captureSelfie.modalPresentationStyle = UIModalPresentationStyle.fullScreen
    present(captureSelfie, animated: true)
``` 

- SACaptureVideo kullanımı:


``` 
        let captureSelfie = SACaptureVideo()
        captureSelfie.delegate = self


        // to show what the user should do in the selected steps, set gifs files
        let gifParams = SAGIFParams()
        gifParams.enabled = true
        gifParams.introFileName = "intro.gif"
        gifParams.turnHeadRightFileName = "turn_right.gif"
        gifParams.turnHeadLeftFileName = "turn_left.gif"
        gifParams.blinkEyesFileName = "blink_eyes.gif"
    

        // use SD quality for screen recording. otherwise, the size of the video may be big
       

        // NOTE 16: add these codes to capture Video - change strings to customize it
        let videoCaptureParams = SACaptureVideoParams()
        // If you want to capture a video for audible confirmation, please set it YES
        // Otherwise, set it NO to capture only selfie video
        videoCaptureParams.enabled = true
        videoCaptureParams.videoNavBarTitle = SALocalizedString("Video Kaydet", "Record video")
        // use the same file name with selfie
        videoCaptureParams.folderNameForVideo = SA_SELFIE_PAGE_FILE_NAME
        // to show what the user should say for voice authorization
        videoCaptureParams.desiredSentence = SALocalizedString("Vermiş olduğum bilgilerin doğruluğunu ve belgelerin bana ait olduğunu onaylıyorum.", "Desired sentence")
        // to explain how the user can use the button for video recording
        videoCaptureParams.recordButtonDescription = SALocalizedString("Sesli yönlendirme bittikten sonra Kayıt Tuşu'nu kullanarak kaydınızı gerçekleştirebilirsiniz.", "Record button description")

        let ttsParams = SATTSParams()
        // if you want to use a custom voice for text to speech, set it yes. otherwise, the default speech engine of the phone will be used
        ttsParams.customTTS = false
      
        // if you want to use a custom voice, do not forget to add the audio file of this custom voice to your project
        let config = SAConfig.create()
        config?.language = "tr"
        ttsParams.customTTSFileName = ((config?.language == "en") ? "custom_male_tts_en.mp3" : "custom_male_tts_tr.mp3")
        // what the tts engine should say to direct the user
        ttsParams.ttsSentence = SALocalizedString("Lütfen kayıt tuşuna tıkladıktan sonra ekranın altında yer alan cümleyi sesli olarak ve kameraya bakarak söyleyiniz.", "TTS sentence")
        // the baseline pitch at which the utterance will be spoken
        ttsParams.pitch = 1.0
        // the rate at which the utterance will be spoken. use the default speech rate of the phone. you should add AVFoundation framework to use it
        ttsParams.speechRate = AVSpeechUtteranceDefaultSpeechRate
        videoCaptureParams.ttsParams = ttsParams

        // use WAV format for the speech recognition service
        videoCaptureParams.audioFormat = .WAV
        // use SD quality for video recording. otherwise, the size of the video may be big
        videoCaptureParams.videoQuality = .standard
        // to detect face in the recorded video
        videoCaptureParams.faceDetectionInVideo = true
        captureSelfie.captureVideoParams = videoCaptureParams

        // If you want to capture a selfie video for verifying human, please set captchaMode YES
        let captcha = SACaptcha()
        // If you enable captchaMode, the numberOfSteps variable will be two
        // And these two steps are turnHeadRight and turnHeadLeft
        captcha.enableCaptchaMode = false
        // If you enable captchaMode, you can dismiss the face recognition listener by setting enableFaceRecognition NO
        // If captchaMode is not enabled, the enableFaceRecognition variable will not work
        captcha.enableFaceRecognition = false
   
        captureSelfie.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        captureSelfie.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        present(captureSelfie, animated: true)
``` 

- Selfie ve Video adımında aşağıdaki delegate fonksiyonları döner.
```
    func captureSelfieOnFaceRecognition(onSelfie screenRecordPath: String!, withScreenRecordFileType screenRecordFileType: String!, withScreenRecordMimeType screenRecordMimeType: String!, withSelfieData selfieData: Data!, withFace faceData: Data!, with faceRecognition: SAFaceRecognition!) {}
    
    func captureSelfieOnFaceRecognition(onVideo imageData: Data!, withFace faceData: Data!, with faceRecognition: SAFaceRecognition!) {}
    
 
    
    func captureVideoDidCancel(_ controller: SACaptureVideo!) {}
    
    func captureVideoDidDone(_ controller: SACaptureVideo!, with captureVideoResult: SACaptureVideoResult!) {}
    
    func captureVideo(onSpeechRecognition videoPath: String!, withVideoFileType videoFileType: String!, withVideoMimeType videoMimeType: String!, withAudioPath audioPath: String!, withAudioFileType audioFileType: String!, withAudioMimeType audioMimeType: String!, withAudioSampleRate audioSampleRate: Int32, with speechRecognition: SASpeechRecognition!) {}
    
    func captureSelfieDidCancel(_ controller: SACaptureSelfie!) {}
    
    func captureSelfieDidDone(_ controller: SACaptureSelfie!, withScreenRecordPath screenRecordPath: String!, withScreenRecordFileType screenRecordFileType: String!, withScreenRecordMimeType screenRecordMimeType: String!, withSelfiePath selfiePath: String!, withFacePath facePath: String!, withIsEncrypted isEncrypted: Bool, with videoCaptureResult: SACaptureVideoResult!) {}
    
    

    
    func captureSelfie(onSpeechRecognition videoPath: String!, withVideoFileType videoFileType: String!, withVideoMimeType videoMimeType: String!, withAudioPath audioPath: String!, withAudioFileType audioFileType: String!, withAudioMimeType audioMimeType: String!, withAudioSampleRate audioSampleRate: Int32, with speechRecognition: SASpeechRecognition!) {}
```


## 3 - SAViewPdf PDF imzalama ve görüntüleme ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : Controllers/SAMobileCaptureModules/SAViewPdfExample.swift

### 2a - PDF imzalama ve görüntüleme kullanımı ###


- PDF imzalama ve görüntülemeyle ilgili örnek kodlar aşağıdaki gibidir:

```
    let tipConfig = SATipConfig.create()
    tipConfig?.tipEnabled = false
    
    let config = SAConfig.create()
    let uiConfig = SAUIConfig.create()

    let viewPdfParams = SAViewPdfParams()
    viewPdfParams!.navBarTitle = (config?.language == "en") ? "Contract" : "Sözleşme"
    viewPdfParams!.backgroundColor = uiConfig?.viewColor
   
    viewPdfParams!.buttonFontFamily = uiConfig?.boldFont
    viewPdfParams!.buttonFontSize = 18.0
    viewPdfParams!.signButtonTitle = (config?.language == "en") ? "Sign Contract" : "Sözleşmeyi İmzala"
    viewPdfParams!.confirmButtonTitle = (config?.language == "en") ? "Confirm" : "Onayla"
    viewPdfParams!.infoLabelBackgroundColor = uiConfig?.navBarColor
    viewPdfParams!.infoLabelTextColor = UIColor.white
    viewPdfParams!.infoLabelFontFamily = uiConfig?.boldFont
    viewPdfParams!.infoLabelFontSize = 16.0
    viewPdfParams!.tipMessage = (config?.language == "en") ? "In order to be able to sign the contract, please check all the pages of the contract." : "Sözleşmeyi imzalamayabilmek için lütfen sözleşmenin bütün sayfalarına göz atınız."
    

    let signDocumentParams = SASignDocumentParams()
    signDocumentParams!.signPadColor = uiConfig?.viewColor
   
    signDocumentParams!.buttonFontFamily = uiConfig?.boldFont
    signDocumentParams!.buttonFontSize = 18.0
    signDocumentParams!.clearButtonTitle = (config?.language == "en") ? "Clear" : "Temizle"

    signDocumentParams!.completeButtonTitle = (config?.language == "en") ? "Complete" : "Tamamla"
    signDocumentParams!.descriptionLabelTextColor = UIColor.red
    signDocumentParams!.descriptionLabelFontFamily = uiConfig?.boldFont
    signDocumentParams!.descriptionLabelFontSize = 18.0
    signDocumentParams!.agreementLabelTextColor = UIColor.darkGray
    signDocumentParams!.agreementLabelFontFamily = uiConfig?.boldFont
    signDocumentParams!.agreementLabelFontSize = 16.0
    signDocumentParams!.agreementLabelText = (config?.language == "en") ? "I agree to sign the contract." : "Sözleşmeyi imzalamayı kabul ediyorum."
    signDocumentParams!.lineColor = UIColor.darkGray
    signDocumentParams!.cancelButtonColor = uiConfig?.navBarColor
    signDocumentParams!.signatureColor = UIColor.black
    signDocumentParams!.invalidSignatureMessage = (config?.language == "en") ? "Invalid Signature" : "Geçersiz İmza"

    let signatureFields = SASignatureFields()

    signatureFields!.addSignatureField(SASignatureField(pageNumber: 1, withBoundingBox: CGRect(x: 330.0, y: 150.0, width: 50.0, height: 50.0)))
    
    signatureFields!.addSignatureField(SASignatureField(pageNumber: 4, withBoundingBox: CGRect(x: 430.0, y: 100.0, width: 60.0, height: 60.0)))
    

    
    

    let viewPdf = SAViewPdf.init(pdfFilePath: Bundle.main.path(forResource: "contract", ofType: "pdf")!, withPdfPassword: nil, with: signatureFields!, withSignedPdfFileName: "contract")
    

    

    viewPdf.delegate = self
    viewPdf.viewPdfParams = viewPdfParams!
    viewPdf.signDocumentParams = signDocumentParams!
    viewPdf.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    viewPdf.modalPresentationStyle = UIModalPresentationStyle.fullScreen

        present(viewPdf, animated: true)
```

- SAViewPdf ile birlikte aşağıdaki delegate fonksiyonları döner 

```
    func onPdfDesign(_ controller: SAViewPdf, withSignAndConfirmButton signAndConfirmButton: UIButton) {}
    
    func onSignDesign(_ controller: SASignDocument, withClear clearButton: UIButton, withComplete completeButton: UIButton) {}
    
    func viewPdfDidCancel(_ controller: SAViewPdf) {}
    
    func viewPdfDidError(_ controller: SAViewPdf, withMessage errorMessage: String) {}
    
    func viewPdfDidDone(_ controller: SAViewPdf) {}
    
    func viewPdfDidDone(_ controller: SAViewPdf, withSignedPdfFilePath signedPdfFilePath: String) {}
```
