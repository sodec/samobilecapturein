/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@class SAAddressPicker;

@protocol SAAddressPickerDelegate <NSObject>

@required
- (void)addressPickerDidCancel:(SAAddressPicker *)controller;
- (void)addressPickerDidDone:(SAAddressPicker *)controller withSelectedItem:(NSString *)selectedItem withSubItemsOfSelectedItem:(NSArray *)subItemsOfSelectedItem withSubItemsAvailable:(BOOL)subItemsAvailable;

@end

@interface SAAddressPicker : UIViewController
{
    id<SAAddressPickerDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SAAddressPickerDelegate> delegate;

- (instancetype)initWithAddressItems:(NSArray *)addressItems withParentItem:(NSString *)parentItem withSubItemsAvailable:(BOOL)subItemsAvailable;

@end
