/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#import <SAMobileCapture/SAApiManager.h>

#define SA_IDENTITY_FOLDER_NAME @"sa_identity"

@interface SACustomerVerification : UIViewController
{
    id<SACustomerVerificationDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SACustomerVerificationDelegate> delegate;
@property (strong, nonatomic, readwrite) SAApiManager *apiManager;
@property (strong, nonatomic, readwrite) NSString *identityNumber;

@end
