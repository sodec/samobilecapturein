/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@class SARequestParams;
@class SACustomerData;
@class SAFormDataList;
@class SAAddress;
@class SAIdentityData;
@class SASecurityItems;
@class SAChipReaderResult;
@class SAVerificationResult;
@class SAKycResult;

@interface SAServiceBuilder : NSObject

+ (void)getAddressService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout success:(void(^_Nonnull)(NSArray * _Nonnull addressItems))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

+ (void)startProcessService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout scenario:(NSString * _Nonnull)scenario success:(void(^_Nonnull)(NSString * _Nonnull cookieOfSession))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

+ (void)getCustomerDataService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout cookieOfSession:(NSString * _Nonnull)cookieOfSession identityNumber:(NSString * _Nonnull)identityNumber success:(void(^_Nonnull)(SACustomerData * _Nonnull customerData))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

+ (void)uploadFilesService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout cookieOfSession:(NSString * _Nonnull)cookieOfSession formDataList:(SAFormDataList * _Nonnull)formDataList success:(void(^_Nonnull)(void))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

+ (void)uploadDataService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout cookieOfSession:(NSString * _Nonnull)cookieOfSession customerData:(SACustomerData * _Nonnull)customerData address:(SAAddress * _Nonnull)address identityData:(SAIdentityData * _Nonnull)identityData securityItems:(SASecurityItems * _Nullable)securityItems chipReaderResult:(SAChipReaderResult * _Nullable)chipReaderResult verificationResultForChip:(SAVerificationResult * _Nullable)verificationResultForChip verificationResultForSelfie:(SAVerificationResult * _Nonnull)verificationResultForSelfie success:(void(^_Nonnull)(void))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

+ (void)finishProcessService:(SARequestParams * _Nonnull)requestParams timeout:(NSTimeInterval)timeout cookieOfSession:(NSString * _Nonnull)cookieOfSession kycResult:(SAKycResult * _Nonnull)kycResult success:(void(^_Nonnull)(void))sucessCallback failure:(void(^_Nonnull)(NSString * _Nonnull errorCode, NSString * _Nonnull errorMessage))failureCallback;

@end
