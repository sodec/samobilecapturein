/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@class SAApiManager;
@class SAIdentityData;
@class SACustomerData;
@class SASecurityItems;
@class SADate;

API_AVAILABLE(ios(13))
@interface SAChipVerification : UIViewController

@property (strong, nonatomic, readwrite) SAApiManager *apiManager;
@property (strong, nonatomic, readwrite) SAIdentityData *fullIdentityData;
@property (strong, nonatomic, readwrite) NSString *identityFaceId;
@property (strong, nonatomic, readwrite) NSString *sessionCookie;
@property (strong, nonatomic, readwrite) SACustomerData *customerData;
@property (strong, nonatomic, readwrite) SASecurityItems *securityItems;

- (instancetype)initWithBirthDate:(SADate *)birthDate withSerialNumber:(NSString *)serialNumber withExpirationDate:(SADate *)expirationDate withIsIDCardTurkish:(BOOL)isIDCardTurkish;

@end
