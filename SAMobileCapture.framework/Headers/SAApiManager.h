/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#import <SAMobileCapture/SADefineVerificationResult.h>
#import <SAMobileCapture/SADefineButtonAction.h>

@class SACustomerVerification;

@protocol SACustomerVerificationDelegate <NSObject>

@required
- (void)customerVerificationDidCancel:(SACustomerVerification *)controller;
- (void)customerVerificationDidDone:(SACustomerVerification *)controller withVerificationResultType:(SAVerificationResultType)verificationResultType withButtonActionType:(SAButtonActionType)buttonActionType;
- (void)customerVerificationDidError:(SACustomerVerification *)controller withErrorMessage:(NSString *)errorMessage;

@end

@interface SAApiManager : NSObject

- (id)initWithBaseUrl:(NSString *)baseUrl withSubscriptionId:(NSString *)subscriptionId withSubscriptionKey:(NSString *)subscriptionKey;
- (id)initWithBaseUrl:(NSString *)baseUrl withSubscriptionId:(NSString *)subscriptionId withSubscriptionKey:(NSString *)subscriptionKey withTimeout:(NSTimeInterval)timeout;

@property (strong, nonatomic, readonly) NSString *baseUrl;
@property (strong, nonatomic, readonly) NSString *subscriptionId;
@property (strong, nonatomic, readonly) NSString *subscriptionKey;
@property (nonatomic, assign, readwrite) NSTimeInterval timeout;
@property (strong, nonatomic, readwrite) NSString *scenario;
@property (nonatomic, assign, readwrite) BOOL showServiceErrors;

- (SACustomerVerification *)createCustomerVerification:(id<SACustomerVerificationDelegate>)customerVerificationDelegate withIdentityNumber:(NSString *)identityNumber;

@end
