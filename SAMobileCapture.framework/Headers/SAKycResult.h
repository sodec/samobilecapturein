/*
* Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Hasan Dertli <hasan.dertli@sodecapps.com>
*
* Sodec Apps Bilisim Teknolojileri
* http://sodecapps.com
* support@sodecapps.com
*/

@class SAKycProcessResult;

@interface SAKycResult : NSObject

- (id)initWithStatus:(BOOL)status withIdentityVerificationProcessResult:(SAKycProcessResult *)identityVerificationProcessResult withChipVerificationProcessResult:(SAKycProcessResult *)chipVerificationProcessResult;

@property (nonatomic, assign) BOOL status;
@property (strong, nonatomic, readwrite) SAKycProcessResult *identityVerificationProcessResult;
@property (strong, nonatomic, readwrite) SAKycProcessResult *chipVerificationProcessResult;

- (NSDictionary *)toDictionary;

@end
