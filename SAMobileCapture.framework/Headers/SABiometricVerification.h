/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

#define SA_SELFIE_FOLDER_NAME @"sa_selfie"

@class SAApiManager;
@class SAIdentityData;
@class SACustomerData;
@class SASecurityItems;
@class SAChipReaderResult;
@class SAVerificationResult;

@interface SABiometricVerification : UIViewController

@property (strong, nonatomic, readwrite) SAApiManager *apiManager;
@property (strong, nonatomic, readwrite) SAIdentityData *fullIdentityData;
@property (strong, nonatomic, readwrite) NSString *identityFaceId;
@property (strong, nonatomic, readwrite) NSString *sessionCookie;
@property (strong, nonatomic, readwrite) SACustomerData *customerData;
@property (strong, nonatomic, readwrite) SASecurityItems *securityItems;
@property (strong, nonatomic, readwrite) SAChipReaderResult *chipReaderResult;
@property (strong, nonatomic, readwrite) NSString *chipFaceId;
@property (strong, nonatomic, readwrite) SAVerificationResult *chipVerificationResult;

@end
