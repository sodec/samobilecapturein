/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@interface SACropView : UIView

@property (nonatomic, strong) UIView *activePoint;
@property (strong, nonatomic) UIView *pointD;
@property (strong, nonatomic) UIView *pointC;
@property (strong, nonatomic) UIView *pointB;
@property (strong, nonatomic) UIView *pointA;
@property (strong, nonatomic) UIView *pointE;
@property (strong, nonatomic) UIView *pointF;
@property (strong, nonatomic) UIView *pointG;
@property (strong, nonatomic) UIView *pointH;
@property (nonatomic, strong) NSMutableArray *points;

- (BOOL)frameEdited;
- (BOOL)isInvalidRect;
- (void)resetFrame;
- (void)expandFrame;
- (CGPoint)coordinatesForPoint:(int)point withScaleFactor:(CGFloat)scaleFactor;

- (void)bottomLeftCornerToCGPoint:(CGPoint)point;
- (void)bottomRightCornerToCGPoint:(CGPoint)point;
- (void)topRightCornerToCGPoint:(CGPoint)point;
- (void)topLeftCornerToCGPoint:(CGPoint)point;

- (void)checkangle:(int)index;
- (void)findPointAtLocation:(CGPoint)location;
- (void)moveActivePointToLocation:(CGPoint)locationPoint withParentLocation:(CGPoint)parentLocationPoint;

- (void)hideZoom;
- (UIView *)getViewForTip;

@end
