/*
* Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Hasan Dertli <hasan.dertli@sodecapps.com>
*
* Sodec Apps Bilisim Teknolojileri
* http://sodecapps.com
* support@sodecapps.com
*/

@interface SASecurityItems : NSObject

- (id)initWithHologramDetectionScore:(float)hologramDetectionScore withGhostDetectionScore:(float)ghostDetectionScore withSignatureDetectionScore:(float)signatureDetectionScore;

@property (nonatomic, assign) float hologramDetectionScore;
@property (nonatomic, assign) float ghostDetectionScore;
@property (nonatomic, assign) float signatureDetectionScore;

- (NSDictionary *)toDictionary;

@end
